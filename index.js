// console.log("Hello World")

// Section: Document Object Model (DOM)
	// allows us to access or modify the properties of an html element in a webpage 
	// It is standard on how to get, change, add, or delete HTML elements
	// we will focus on use of DOM in managing forms

// For selecting HTML elements we will be using document.querySelector
	// Syntax: document.querySelector("html element")
	// the querySelector function takes a string input that is formatted like a css selector whhen applying 

	// Reads elements from to top to bottom
	const txtFirstName = document.querySelector("#txt-first-name");
	// console.log(txtFirstName);

	const txtLastName = document.querySelector("#txt-last-name");


	// finds all class attribute with full-name
	const name = document.querySelectorAll(".full-name");
	// console.log(name);

	const span = document.querySelectorAll("span")
	// console.log(span)
	// input element with attribute of type
	const text = document.querySelectorAll("input[type]")
	// console.log(text)

	const div = document.querySelectorAll(".first-div > span")
	// console.log(div)

	const spanFullName = document.querySelector("#full-name")

	const changeColor = document.querySelector("#color")




// target specific using parent > child
/*	const div1 = document.querySelector(".first-div > .second-span")
	console.log(div)*/

// Section Event Listeners4
	// Whenever a user interacts with a webpage, this action is considered as an event.
	// Working with events is a large part of creating interactivity in a webpage.
	// specific functions that will perform an action

// The function use is "addEventListener", it takes two arguments
	// first argument a string identifying an event
	// second argument, function that the listener will trigger once the "specified event" is triggered

	// keyup release of keyboard
	// keydown once click/ press

/*	txtFirstName.addEventListener("keyup", (event)=>{
		// if no target.value it will return an object, if there has it will only return the value 
		console.log(event.target.value);
	})*/

	

	const fullName = ()=> {
		spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value} `
	}

	txtFirstName.addEventListener("keyup", (fullName))

	txtLastName.addEventListener("keyup", (fullName))

	const fullColor =() => {
		spanFullName.style.color = `${changeColor.value}` 
	}

	changeColor.addEventListener("click", fullColor)

